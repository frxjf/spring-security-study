package com.xjf.security.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xjf
 * @date 2020/2/20 16:00
 */
@RestController
@Slf4j
public class SmsController {

    /**
     * 发送验证码
     *
     * @param mobile
     * @param session
     * @return
     */
    @GetMapping("/sms/code")
    public String sms(String mobile, HttpSession session){
        int code = (int)Math.ceil(Math.random() * 9000 + 1000);

        Map<String, Object> map = new HashMap<>(16);
        map.put("mobile", mobile);
        map.put("code", code);
        // 将手机号和生成的验证码放到 session 中
        session.setAttribute("smsCode", map);
        log.info("{}：为 {} 设置短信验证码：{}", session.getId(), mobile, code);

        return "你的手机号：" + mobile + " 验证码是：" + code;
    }
}
