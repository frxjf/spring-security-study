package com.xjf.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author xjf
 * @date 2020/2/20 10:19
 */
@Controller
public class TestController {

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    /**
     * 路径跳转
     * @return
     */
    @GetMapping("/login")
    public String login(){
        return "login2";
    }
}
