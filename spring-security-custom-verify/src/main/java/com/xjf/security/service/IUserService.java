package com.xjf.security.service;

import com.xjf.security.entity.SysUser;

/**
 * @author xjf
 * @date 2020/2/20 10:22
 */
public interface IUserService {

    SysUser findByUsername(String username);
}
