package com.xjf.security.entity;

import lombok.Data;

import java.util.List;

/**
 * @author xjf
 * @date 2020/2/20 10:20
 */
@Data
public class SysUser {

    private Long id;

    private String username;

    private String password;

    private List<String> roles;

    public SysUser() {
    }

    public SysUser(Long id, String username, String password, List<String> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}
