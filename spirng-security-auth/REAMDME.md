### 介绍:
1. 该项目测试 spring-security 的权限控制
2. 代码实现权限控制，实现原理：
    1. 在 UserService 进行登录用户信息获取后，将权限也注入到 org.springframework.security.core.userdetails.User 中
    2. 在 WebSecurityConfig 中配置时，对不同的路径分配不同的角色，以此来实现不同权限的不同资源访问
    3. 在配置时，可以使用 .access 来自定义判断。
3. 注解实现权限控制，实现方法：
    1. 在 WebSecurityConfig 中添加一下注解开启注解的使用
        ```java
        @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
        ```
    2. 在要使用的方法上添加注解就行，查看 PermissionController .
    3. 启动可以使用自定义的权限判断

#### 学习
1. 查看博客: https://blog.csdn.net/qq_32867467/category_9047805.html