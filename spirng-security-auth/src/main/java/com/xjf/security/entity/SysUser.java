package com.xjf.security.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户
 *
 * @author xjf
 * @date 2020/2/21 13:57
 */
@Data
public class SysUser implements Serializable {

    private Long id;

    private String username;

    private String password;

    private List<SysRole> roles;

    public SysUser(Long id, String username, String password, List<SysRole> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
}
