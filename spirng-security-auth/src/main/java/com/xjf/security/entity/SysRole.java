package com.xjf.security.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 角色
 *
 * @author xjf
 * @date 2020/2/21 13:56
 */
@Data
public class SysRole implements Serializable {
    private Long id;

    private String roleName;

    private List<String> permissions;

    public SysRole(Long id, String roleName, List<String> permissions) {
        this.id = id;
        this.roleName = roleName;
        this.permissions = permissions;
    }
}
