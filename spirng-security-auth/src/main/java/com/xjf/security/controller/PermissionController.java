package com.xjf.security.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xjf
 * @date 2020/2/21 14:13
 */
@RestController
public class PermissionController {

    @PreAuthorize("hasRole('ROLE_DOCKER')")
    @RequestMapping("/docker")
    public String test1() {
        return "说明你有docker权限";
    }

    @PreAuthorize("@testPermissionEvaluator.check(authentication)")
    @RequestMapping("/custom")
    public String test0() {
        return "说明你有自定义权限";
    }

    @PreAuthorize("hasRole('ROLE_JAVA')")
    @RequestMapping("/java")
    public String test2() {
        return "说明你有java权限";
    }

    @PreAuthorize("hasRole('ROLE_PHP')")
    @RequestMapping("/php")
    public String test3() {
        return "说明你有最好语言的权限";
    }
}
