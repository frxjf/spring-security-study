package com.xjf.security.service.impl;

import com.xjf.security.entity.SysUser;
import com.xjf.security.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author xjf
 * @date 2020/2/20 10:23
 */
@Service
public class UserServiceImpl implements IUserService {

    /**
     * 模拟数据库数据
     */
    private static final Set<SysUser> users = new HashSet<>();


    static {
        users.add(new SysUser(1L, "xjf", "123456", Arrays.asList("ROLE_ADMIN", "ROLE_DOCKER")));
        users.add(new SysUser(2L, "dale", "123456", Arrays.asList("ROLE_ADMIN", "ROLE_DOCKER")));
        users.add(new SysUser(3L, "咖啡", "123456", Arrays.asList("ROLE_ADMIN", "ROLE_DOCKER")));
    }

    @Override
    public SysUser findByUsername(String username) {
        return users.stream().filter(sysUser -> username.equals(sysUser.getUsername())).findFirst().orElse(null);
    }
}
