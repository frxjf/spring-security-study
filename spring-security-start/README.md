### 介绍：
1. 该项目主要用来学习 SpringSecurity ， 第一个启动 demo。 快速入门。
2. 在引入 SpringSecurity 的 pom 包后，启动项目访问接口时就会被拦截。并且被重定向到登录页面。要想登录，默认的账号密码是：账号： user ；
    密码：在项目启动时会自动生成一个密码串（比如: 11bbd2ff-8135-43d5-bb7e-d8a40b116a6e)。可以直接用来登录
3. Spring Security 支持两种不同的认证方式：
   - 可以通过 form 表单来认证
   - 可以通过 HttpBasic 来认证
#### 原理
1. 编写 UserService 实现接口 UserDetailsService 。主要作用是对登录用户根据用户名去数据查询信息，并将密码、权限等信息交给 SpringSecurity。
2. 编写两个处理类：
    - 实现 AuthenticationFailureHandler 接口，主要是在登录失败时执行，可以设置返回信息为 JSON。
    - 实现 AuthenticationSuccessHandler 接口，在登录成功时执行，可以设置返回信息为 JSON。
3. 编写 WebSecurityConfig 类继承自 WebSecurityConfigurerAdapter 类。主要工作如下：
    1. 将我们实现的 userService 放入 AuthenticationManagerBuilder ， 并可以对密码进行加密和匹配
    2. 对登录认证进行设置，设置登录 url ，成功和失败的处理器，不拦截的路径等。
4. 引入 thymeleaf 模板引擎，实现页面的跳转等。可以加如下的配置：
    ```yaml
    spring:
      thymeleaf:
        # 关闭 thymeleaf 的缓存
        cache: false
        # 关闭对 html 进行严格的语法校验，需要引入 pom 文件 nekohtml 配合使用
        mode: LEGACYHTML5
    ```
#### 实现记住我功能
1. 实现方式有两种：
    - cookie 方式
    - 数据库存储方式
2. 测试方法为：勾选 "记住我" 登录后，关闭浏览器再次访问需要登录才能访问的页面，如果可以直接访问，则代表实现成功，否则为失败。
3. cookie 方式：
    1. 在登录表单添加一个 checkbox ，选中传 true ，未选中传 false。传值属性为 remember-me.
    2. 在 WebSecurityConfig 中添加对 remember 的配置，如下：
        ```java
           http.rememberMe()
            // 修改 cookie 的属性值，默认是 remember-me
            // .rememberMeCookieName("remember")
            .tokenValiditySeconds(3600)
        ```
4. 数据库存储方式，在客户端的 Cookie 中，仅保存一个无意义的加密串（与用户名、密码等敏感数据无关），然后在数据库中保存该加密串-用户信息的对应关系，
    自动登录时，用 Cookie 中的加密串，到数据库中验证，如果通过，自动登录才算通过。实现：
    1. 编写 RememberMeConfig , 其中 tokenRepository.setCreateTableOnStartup(true); 可以自动生成表。
    2. 配置数据源
    3. 在 WebSecurityConfig 中添加如下，加入我们的配置类：
        ```java
        .tokenRepository(persistentTokenRepository)
        ```
    4. 此时可以测试就可以了